<?php

namespace Lbonsu\CustomInventory\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Lbonsu\CustomInventory\Helper\Data;

/**
 * Class CatalogProductSaveAfterObserver
 * @package Lbonsu\CustomInventory\Observer
 */
class CatalogProductSaveAfterObserver implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $customInventoryHelper;

    /**
     * CatalogProductSaveAfterObserver constructor.
     * @param Data $customInventoryHelper
     */
    public function __construct(Data $customInventoryHelper)
    {
        $this->customInventoryHelper = $customInventoryHelper;
    }

    /**
     * Get new stock quantity and save
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getProduct();
        $origData = $product->getOrigData();

        $this->customInventoryHelper->saveInventoryEntry(
            $product->getSku(),
            $product['stock_data']['qty'],
            $origData['quantity_and_stock_status']['qty'],
            $product->getUpdatedAt());
    }
}