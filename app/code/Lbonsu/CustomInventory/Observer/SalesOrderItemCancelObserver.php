<?php

namespace Lbonsu\CustomInventory\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Lbonsu\CustomInventory\Helper\Data;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class SalesOrderItemCancelObserver
 * @package Lbonsu\CustomInventory\Observer
 */
class SalesOrderItemCancelObserver implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $customInventoryHelper;
    /**
     * @var StockStateInterface
     */
    protected $stockItem;
    /**
     * @var DateTime
     */
    protected $datetime;

    /**
     * SalesOrderItemCancelObserver constructor.
     * @param Data $customInventoryHelper
     * @param StockStateInterface $stockItem
     * @param DateTime $datetime
     */
    public function __construct(
        Data $customInventoryHelper,
        StockStateInterface $stockItem,
        DateTime $datetime
    ){
        $this->customInventoryHelper = $customInventoryHelper;
        $this->stockItem = $stockItem;
        $this->datetime = $datetime;
    }

    /**
     * Get stock quantity from the order item and save
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $skipProductType = array('configurable', 'bundle');
        $orderItem = $observer->getItem();

        // Save simple types only
        if(in_array($orderItem->getProductType(), $skipProductType)){return;}

        /* We'll have to use $orderItem->getQtyOrdered() rather than
           $orderItem->getQtyCanceled() because we can't specify the execution
           order of observers and "sales_order_item_cancel_after" doesn't exist */
        $qty = $this->stockItem->getStockQty($orderItem->getProductId());
        $origQty = ($qty - $orderItem->getQtyOrdered());

        $this->customInventoryHelper->saveInventoryEntry(
            $orderItem->getSku(),
            $qty,
            $origQty,
            $this->datetime->gmtDate());
    }
}