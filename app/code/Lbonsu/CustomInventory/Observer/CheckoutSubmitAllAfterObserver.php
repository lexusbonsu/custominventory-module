<?php

namespace Lbonsu\CustomInventory\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Lbonsu\CustomInventory\Helper\Data;
use Magento\CatalogInventory\Api\StockStateInterface;

/**
 * Class CheckoutSubmitAllAfterObserver
 * @package Lbonsu\CustomInventory\Observer
 */
class CheckoutSubmitAllAfterObserver implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $customInventoryHelper;
    /**
     * @var StockStateInterface
     */
    protected $stockItem;

    /**
     * CheckoutSubmitAllAfterObserver constructor.
     * @param Data $customInventoryHelper
     * @param StockStateInterface $stockItem
     */
    public function __construct(Data $customInventoryHelper, StockStateInterface $stockItem)
    {
        $this->customInventoryHelper = $customInventoryHelper;
        $this->stockItem = $stockItem;
    }

    /**
     * Get stock quantity from each simple order item
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderItems = $order->getItems();
        $skipProductType = array('configurable', 'bundle');

        // Save simple types only
        foreach ($orderItems as $orderItem) {
            if(in_array($orderItem->getProductType(), $skipProductType)){continue;}
            $qty = $this->stockItem->getStockQty($orderItem->getProductId());
            $origQty = ($qty + $orderItem->getQtyOrdered());

            $this->customInventoryHelper->saveInventoryEntry(
                $orderItem->getSku(),
                $qty,
                $origQty,
                $order->getCreatedAt());

        }
    }
}