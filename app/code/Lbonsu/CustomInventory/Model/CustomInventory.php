<?php
/**
 * Created by PhpStorm.
 * User: lexus
 * Date: 17/03/18
 * Time: 14:28
 */

namespace Lbonsu\CustomInventory\Model;

/**
 * @method \Lbonsu\CustomInventory\Model\ResourceModel\CustomInventory getResource()
 * @method \Lbonsu\CustomInventory\Model\ResourceModel\CustomInventory\Collection getCollection()
 */
class CustomInventory extends \Magento\Framework\Model\AbstractModel implements \Lbonsu\CustomInventory\Api\Data\CustomInventoryInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'lbonsu_custominventory_custominventory';
    protected $_cacheTag = 'lbonsu_custominventory_custominventory';
    protected $_eventPrefix = 'lbonsu_custominventory_custominventory';

    protected function _construct()
    {
        $this->_init('Lbonsu\CustomInventory\Model\ResourceModel\CustomInventory');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}