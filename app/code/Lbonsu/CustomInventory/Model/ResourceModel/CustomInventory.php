<?php
/**
 * Created by PhpStorm.
 * User: lexus
 * Date: 17/03/18
 * Time: 14:28
 */

namespace Lbonsu\CustomInventory\Model\ResourceModel;

class CustomInventory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('lbonsu_custominventory', 'lbonsu_custominventory_id');
    }

}