<?php
/**
 * Created by PhpStorm.
 * User: lexus
 * Date: 17/03/18
 * Time: 14:28
 */

namespace Lbonsu\CustomInventory\Model\ResourceModel\CustomInventory;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'custominventory_id';


    protected function _construct()
    {
        $this->_init('Lbonsu\CustomInventory\Model\CustomInventory',
            'Lbonsu\CustomInventory\Model\ResourceModel\CustomInventory');
    }

}