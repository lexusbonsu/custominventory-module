<?php

namespace Lbonsu\CustomInventory\Helper;

use Lbonsu\CustomInventory\Model\CustomInventoryFactory;

/**
 * Class Data
 * @package Lbonsu\CustomInventory\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var CustomInventoryFactory
     */
    protected $customInventoryFactory;

    /**
     * Data constructor.
     * @param CustomInventoryFactory $customInventoryFactory
     */
    public function __construct(CustomInventoryFactory $customInventoryFactory)
    {
        $this->customInventoryFactory = $customInventoryFactory;
    }

    /**
     * Save inventory updates
     * @param $sku
     * @param $qty
     * @param $originalQty
     * @param $updatedAt
     */
    public function saveInventoryEntry($sku, $qty, $originalQty, $updatedAt)
    {
        $inventoryEntry = $this->customInventoryFactory->create();
        $inventoryEntry->setSku($sku)
            ->setQty($qty)
            ->setVariation(($qty - $originalQty))
            ->setUpdatedAt($updatedAt);
        $inventoryEntry->save();
    }

}
