Possible Problems/Improvements

The issue with this extension is the fact that under heavy-load (mainly with popular promotions), there'll be a large volume of calls to the database with entries from the "checkout_submit_all_after" event.
It would be best to free up as much system resources as possible to customers attempting to complete their orders.

A solution to this problem would be moving all entries from "checkout_submit_all_after" to run on a cron and save them to the database in a batch instead. 
Whenever the cronjob is run or an admin presses the "Update Inventory Records" button, the extension would get all orders created in the past 24 hours (or whenever the cron was last successfully run) and 
save the entries to the db as normal (CheckoutSubmitAllAfterObserver.php)
A caveat to this is that if an admin makes a stock change between two cronjobs, the variation value will be incorrect and wont match with the timestamp.
This can be solved by saving the current stock quantity to the order - something like 'current_stock_qty' and using that value as $origQty.

*** Data from "catalog_product_save_after" and "sales_order_item_cancel" can be saved immeditely since the likelyhood of them creating multiple entries within a short space of time is small.

Another issue is that the size of the custom_inventory table could easily reach 1000+ entries.
In this case it may be best to have a limit of one entry per product per X hours/days.